	$(document).ready(function () {

		$('.minus').click(function () {
			var $input = $(this).parent().find('input');
			var count = parseInt($input.val()) - 1;
			count = count < 1 ? 1 : count;
			$input.val(count);
			$input.change();
			return false;
		});
		$('.plus').click(function () {
			var $input = $(this).parent().find('input');
			$input.val(parseInt($input.val()) + 1);
			$input.change();
			return false;
		});


		$('.control-btn--liked-btn').on('click', function () {
			$(this).toggleClass('favorites');
		});

		$('#choose').click(function () {
			$('.product-list .product input[type="checkbox"]').prop('checked', this.checked);
		})



		$('.select').each(function () {
			var firstItem = $(this).find('.select__option').first().attr('data-value');
			console.log(firstItem);
			$(this).find('.select__input').val(firstItem);
			$(this).find('.select__selected-option').text(firstItem);
		});


		$('.select__option').on('click', function () {
			var option = $(this).attr('data-value');
			$(this).parent().parent().find('.select__input').val(option);
			$(this).parent().parent().find('.select__selected-option').text(option);
		});

		$('.select').on('click', function () {
			$(this).find('.select__dropdown').toggleClass('show');
			$('.select').not(this).find('.select__dropdown').removeClass('show');
		});


		$(document).mouseup(function (e) {
			var div = $(".select");
			if (!div.is(e.target) &&
				div.has(e.target).length === 0) {
				$('.select__dropdown').removeClass('show');
			}
		});

		var swiper = new Swiper("#banner-home", {
			loop: true,
			speed: 1500,
			effect: "fade",
			autoplay: {
				delay: 5000,
				disableOnInteraction: false
			}
		});

		var swiper = new Swiper("#bestsellers", {
			slidesPerView: 6,
			spaceBetween: 35,
			navigation: {
				nextEl: ".bestsellers-next",
				prevEl: ".bestsellers-prev",
			},
			breakpoints: {
				0: {
					slidesPerView: 1,
					spaceBetween: 0
				},
				400: {
					slidesPerView: 2,
					spaceBetween: 5
				},
				480: {
					slidesPerView: 2,
					spaceBetween: 10
				},
				640: {
					slidesPerView: 3,
					spaceBetween: 15
				},
				991: {
					slidesPerView: 4,
					spaceBetween: 20
				},
				1199: {
					slidesPerView: 6,
					spaceBetween: 25
				},

			}
		});

		var swiper = new Swiper(".brands-slider-js", {
			loop: true,
			autoplay: {
				delay: 4500,
				disableOnInteraction: false
			},
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			breakpoints: {
				0: {
					slidesPerView: 3
				},
				400: {
					slidesPerView: 4
				},
				480: {
					slidesPerView: 5
				},
				767: {
					slidesPerView: 8
				},
				992: {
					slidesPerView: 9
				},
				1200: {
					slidesPerView: 10
				},
				1400: {
					slidesPerView: 13
				},
				1600: {
					slidesPerView: 16
				},

			}
		});

		var swiper = new Swiper(".ad-banner-slider-left", {
			loop: true,
			speed: 1500,
			autoplay: {
				delay: 4500,
				disableOnInteraction: false
			}
		});
		var swiper = new Swiper(".ad-banner-slider-right", {
			loop: true,
			speed: 1500,
			autoplay: {
				delay: 5000,
				disableOnInteraction: false
			}
		});

		var swiper = new Swiper(".randoffer", {
			loop: true,
			spaceBetween: 20,
			speed: 5000,
			autoplay: {
				delay: 0,
				disableOnInteraction: false
			},
			breakpoints: {
				0: {
					slidesPerView: 3
				},
				576: {
					slidesPerView: 4
				},
				992: {
					slidesPerView: 5
				},
			}
		});


		var distance = ($(".container").offset().left + 20);

		$('.content-section--float-right .content-section__col-wrap').css('padding-left', distance)


	});


	$(document).ready(function () {

		(function ($) {
			$('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

			$('.tab ul.tabs li .bt').click(function () {
				var tab = $(this).closest('.tab'),
					index = $(this).closest('li').index();

				tab.find('ul.tabs > li').removeClass('current');
				$(this).closest('li').addClass('current');

				tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
				tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();


			});
		})(jQuery);



		$('.additional-desc__more').on('click', function () {
			$(this).prev().toggleClass('--open');
			$(this).hide();
			$('.additional-desc__hide').show();
		});
		$('.additional-desc__hide').on('click', function () {
			$('.additional-desc__more').show();
			$(this).prev().prev().removeClass('--open');
			$(this).hide();
		});

	});